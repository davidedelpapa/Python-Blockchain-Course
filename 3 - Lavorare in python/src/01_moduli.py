# Moduli

# Possiamo pensare ai moduli in Python come a un contenitore di codice che risolve un compito specifico, 
# e che sia riutilizzabile.
# Se per esempio usiamo spesso una funzione per stampare un messaggio in un certo modo, 
# possiamo creare un modulo che svolga questa funzione e riutilizzarlo in tutti i nostri nuovi progetti.

# Se salviamo un file chaiamto *ciao.py* nella cartella *moduli/*, contenete:

# def saluta(nome):
#    print("Ciao, " + nome + '!')

# possimo importare la funzione saluta() nel nostro codice

from moduli.ciao import *

# Con il codice sopra abbiamo importato tutto (* asterisco) dal file ciao nella cartella moduli
# Adesso possiamo usare la funzione come se l'avessimo scritta nello script corrente:

saluta('Antonio')

# Nei moduli possiamo mettere classi, funzioni ed anche variabili da riutilizzare.
# Ad esempio, se aggiungiamo il seguente al file *ciao.py*

# instruttore = {
#     "nome": "Davide",
#     "età": 33
#}

# possiamo usare la variabile *istruttore* nel nostro codice:

saluta(istruttore["nome"])

# Ci sono vari modi per importare moduli:

# si può importare tutto da un modulo come abbiamo visto, e poi usare le funzioni:

from moduli.ciao import *
saluta('Giovanni')

# Vantaggio:
#   - Si usano le variabili, le funzioni, le classi subito
# Svantaggi:
#   - Ci possono essere confilitti di nomi: soprattutto in progetti grandi ci possono essere 
#     più funzioni con lo stesso nome da moduli diversi. Anche se sembra di poco conto, 
#     è sempre meglio sapere da quale modulo una funzione viene chiamata.
#   - qualche volta non è conveniente importare tutto da un modulo, ma solo ciò che serve, 
#     un altro svntaggio che si presenta soprattutto per grandi progetti.

# Si può allo stesso modo importare solo ciò che serve:

from moduli.ciao import saluta
saluta('Pasquale')
#saluta(istruttore["nome"]) # ERRORE! La variabile istruttore non è stata importata!

from moduli.ciao import saluta, istruttore
saluta(istruttore["nome"]) # Adesso sì!

# Vantaggio:
#   - Si usano solo le variabili, le funzioni, le classi che servono, utile per avere le cose chiare
# Svantaggi:
#   - Ci possono essere confilitti di nomi: soprattutto in progetti grandi ci possono essere 
#     più funzioni con lo stesso nome da moduli diversi. Anche se sembra di poco conto, 
#     è sempre meglio sapere da quale modulo una funzione viene chiamata.
#   - Questa forma è abbastanza prolissa: si deve scrivere più codice

# Si può anche importare il modulo intero ed usare ciò che serve

import moduli.ciao
moduli.ciao.saluta("Antonio")

# ovviamente è una forma molto prolissa, ma molto esatta: non c'è possibilità di confusione.
# Per ovviare un po' alla prolissità di questa forma, ma per mantenere comumque la separazione 
# dei "namespae", ovvero i nomi di funzioni separati dai punti 
# (così che ogni nome è valido nel suo spazio, il "namespace"), si può usare il metodo dell'alias:

import moduli.ciao as mod
mod.saluta("Antonio")

# Questa forma è molto usata, specialmente in grandi progetti.

# Vantaggi:
#   - Si usano solo le variabili, le funzioni, le classi che servono, utile per avere le cose chiare
#   - Nessuna confusione, i "namespace" sono "puliti", ovvero nettamente separati
# Svantaggio:
#   - Resta comumque una forma abbastanza prolissa.

# Ma perchè parlo sempre di grandi progetti e mi preoccupo dei "namespace" e di avere le cose chiare?
# Perchè i moduli sono la forza del python: ce ne sono una enorme quantità nell'installazione di base, 
# ed una grandissima quantità è stat creata da programmatori di tutto il mondo e resa disponibile per 
# l'installazione nei propri progetti (vedremo meglio con la prossima lezione)
# La grande disponibilità di moduli che viene con l'istallazion python di base è uno dei 
# punti cardine del linguaggio fin dall'inizio: il motto era "battery included", ovvero, "batterie incluse", 
# una ironia inglese sui giochi che si comprano e poi c'è scritto su "batterie non incluse".
# Questo vuol dire che usando il Python un programmatore si deve concentrare sulla logica del suo programma, 
# non a scrivere codice di compatibilità con il sistema e le varie tecnologie.

# Esempio:

import os # Ci interfacciamo con il sstema operativo
print(os.name) # variabile contenete il nome generico del tipo di sistema: 
               #  nt per windows, posix per Linux, etc..
import platform # platform è più specifico
print(platform.system()) # funzione che ci dà il nome del sistema
print(platform.release()) # funzione che ci dà la versione del sistema

# Molti linguaggi hanno librerie di sistema che arrivino fin qui.
# Ma il python va ben oltre:

import smtplib
from email.message import EmailMessage
msg = EmailMessage()
msg.set_content("Ciao")
msg["Subject"] = "Prova"
msg["From"] = "davidedelpapa@gmail.com"
msg["To"] = "davidedelpapa@gmail.com"

# FINIRE!

# Inoltre il Python è sempre stato all'avanguardia con le nuove tecnologie: ha delle librerie per interfacciarsi con tutto, molte scritte anche dai produttori stessi delle nuove tecnologie: Google, Facebook, Instagram, Raspberry, Amazon, ... l'elenco è vastissimo!





