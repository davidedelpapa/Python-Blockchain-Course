import hashlib
import json

__all__ = ["mine", "hash"]

def mine(block):
    """ Trova la proof_of_work per il blocco """
    nonce = 0
    while check_valid_proof(block, nonce) is False:
        nonce += 1
    return nonce

def check_valid_proof(block, nonce):
    """ Controlla se la proof per il blocco è valida """
    block["nonce"] = str(nonce)
    try_hash = str(hash(block))
    return try_hash[:4] == "0000"

def hash(block):
    """ Esegue l'hash del blocco con SHA-256 """
    raw_block = json.dumps(block, sort_keys=True).encode()
    return hashlib.sha256(raw_block).hexdigest()
