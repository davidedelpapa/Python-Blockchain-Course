import requests
import json
from .mine import mine, hash

class Client(object):
    def __init__(self, url_base, indirizzo_mio):
        self.url_base = url_base
        self.indirizzo_mio = indirizzo_mio
    
    def ping_ok(self):
        """ Funzione per controllare che il server sia online """
        try:
            r = requests.get("{}/".format(self.url_base))
            return r.status_code == 200
        except requests.exceptions.RequestException as e:
            return False    

    def _last_block(self):
        """ Funzione interna per ottenere l'ultimo blocco' """
        try:   
            r = requests.get("{}/last_block".format(self.url_base))
            block = json.loads(r.text)
            return block
        except requests.exceptions.RequestException as e:
            print(e)
            sys.exit(1)   

    def _muovi_denaro(self, da, a, quanto):
        """ 
        Funzione basica di movimento di denaro.
        Le 3 funzioni di movimento dipendono da questa
        """
        try:      
            transaction_parametri = {
                'sender': da, 
                'recipient': a,
                'amount': quanto
            }
            
            r = requests.post("{}/movimento".format(self.url_base), data=transaction_parametri)
            if r.status_code == 200:
                print("Il tuo movimento verrà registrato nel blocco:\t{}\n".format(r.text))
                print("Ultimi movimenti:\n{}".format(self._ultimi_movimenti()))
            
        except requests.exceptions.RequestException as e:
            print(e)
            sys.exit(1)

    def _ultimi_movimenti(self):
        """ Funzioni interna per ottienere gli ultimi movimenti """
        try:            
            r = requests.get("{}/ultimi_movimenti".format(self.url_base))
            if r.status_code == 200:
                return json.loads(r.text)
        except requests.exceptions.RequestException as e:
            print(e)
            sys.exit(1)   

    def miner(self, args):
        """ Funzione di mining """
        try:        
            block = self._last_block()
            
            nonce = mine(block)

            transaction_parametri = {
                'nonce': nonce,
                'recipient': self.indirizzo_mio,
            }

            r = requests.post("{}/mine".format(self.url_base), data=transaction_parametri)
            print("Risposta server al mining corretto:\t{}\n".format(r.text))
            print("Ultimo blocco:\n{}".format(self._last_block()))
            print("Ultime transazioni:\n{}".format(self._ultimi_movimenti()))
        except requests.exceptions.RequestException as e:
            print(e)
            sys.exit(1)

    def movimento(self, args):
        """ Funzione generica di movimento denaro """
        da = args.mov_par[0]
        a = args.mov_par[1]
        quanto = args.mov_par[2]
        
        self._muovi_denaro(da, a, quanto)

    def dai(self, args):
        """ Funzione per dare denaro dal tuo wallet """
        da = self.indirizzo_mio
        a = args.dai_par[0]
        quanto = args.dai_par[1]
        
        self._muovi_denaro(da, a, quanto)

    def ricevi(self, args):
        """ Funzione per ricevere denaro dal tuo wallet """
        da = args.ricevi_par[0]
        a = self.indirizzo_mio
        quanto = args.ricevi_par[1]
        
        self._muovi_denaro(da, a, quanto)
        
    def ultimo_blocco(self, args):
        """ Ottieni l'ultimo blocco scritto """
        print(self._last_block())

    def ultime_transazioni(self, args):
        """ Lista delle ultime transazioni """
        print(self._ultimi_movimenti())
    
    def registra_nodi(self, args):
        """ Registra nodi al server connesso """
        parametri_nodo = {
            'url': args.nuovo_nodo[0],
            'address': args.nuovo_nodo[1]
        }
        
        r = requests.post("{}/nodi/registra".format(self.url_base), json=parametri_nodo)
        print(r.text)

