## Docker

Connetti to [https://hub.docker.com/](https://hub.docker.com/)

In fondo: "Docker editions"

Scegliere *Docker CE*

Scegliere *docker-desktop (windows)*

Scegliere *Please login to download* e in fondo *create account*

Una volta fatto il riconoscimento potremo scaricare docker per windows.

Alla linea di comando (cmd.exe)

```sh
docker pull python
```
