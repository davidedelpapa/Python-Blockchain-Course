# Controllo dell'esecuzione

# L'esecuzione di un programma non può sempre procedere linearmente
# Infatti la maggior parte delle volte in un programma si devono prendere decisioni
# a seconda di alcune condizioni.

# Altre volte invece si devono riprodurre le stesse azioni o gli stessi stati ciclicamente 
# (si pensi all'alternanza dei colori di un semaforo).

# Altre volte ancora bisogna eseguire le stesse azioni in serie per ogni elemento di una collezione,
# come ad esempio stampare a video tutti gli elementi di una lista.

# In tutti questi casi si parla di esecuzione non lineare, e di controllo dell'esecuzione
# Abbiamo quindi esecuzione condizionale (il prendere decisioni) ed esecuzione di cicli(il ripetersi di stati).

# Il python ha vari strumenti per il controllo dell'esecuzione:

# - if          Esecuzione condizionale
# - while       Esecuzione di cicli
# - for         Esecuzione ed applicazione di cicli

# Passeremo in rassegna a breve tutti questi elementi.


