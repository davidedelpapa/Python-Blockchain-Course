# Classi e Oggetti

# Python è un linguaggio orientato agli oggetti.
# Cosa vuol dire?

# La programmazione ad oggetti è una metodologia per creare programmi.
# In realtà questa metodologia sfrutta l'astrazione logica,
# cui siamo tutti abituati, e tenta di applicarlo ai programmi.

# Per capire i vari aspetti inizieremo da degli esempi, 
# più che le forme canoniche, per tentare di dare un approccio più accessibile

# Per prima cosa: Classi ed Oggetti

class classe:
    a = 2       # propietà a della classe
    x = "Ciao"  # propietà x della classe
    
# Instanziamo la classe, cioè creamo oggetti

c1 = classe() # Ci sono parentesi, ma non è una funzione!

# Possiamo accedere alle propietà della classe

print(c1.a)
print(c1.x)

# Possiamo creare altri oggetti a partire dalla stessa classe

c2 = classe()

print((c1.a == c2.a) and (c1.x == c2.x))

# Per capire questo concetto penisamo a una classe come al progetto di una casa;
# nelle villette a schiera lo stesso progetto può essere applicato 
# per creare più case, riutilizzato

# La classe è come fosse il progetto, l'oggetto la casa costruita a partire da tale oggetto
# Ogni oggetto creato ha le stesse propietà della classe, così come 
# ogni casa rispecchia il suo progetto di costruzione


# Proprietà e metodi

# Ogni classe può avere proprietà (variabili sue interne) e metodi (funzioni sue interne)

class cane:
    colore = "non assegnato"
    def abbaia(self): # mettere self qui è d'obbligo se vogliamo chiamarlo
        print("Wof wof!")

fufi = cane()

fufi.colore = "bianco"
fufi.abbaia()

# I metodi già li abbiamo visti all'opera, con le classi di default del linguaggio
# Esempio: stringa.lower() è un metodo della classe str da cui derivano tutte le stringhe


# Metodo __init__()

# Il metodo __init__() è un metodo particolare, che viene chiamato quando la classe 
# viene instanziata (ovvero si crea un oggetto a partire dalla classe)
# Questo metodo è molto utile all'atto pratico per inizializzare i parametri della classe

# __init__() è anche chiamato "costruttore" della classe

class animale():
    def __init__(self, colore, verso):
        self.colore = colore
        self.verso = verso
    def fa_verso(self):
        print(self.verso)

fufi = animale("bianco", "wof wof!")
paperotto = animale("giallo", "quack quack!")

print(fufi.colore)
print(paperotto.colore)

fufi.fa_verso()
paperotto.fa_verso()

# Alcune cose da capire bene:

# - self viene usato per riferire alla classe stessa
#   Quando scriviamo self.proprietà ci riferiamo ad una proprietà della classe stessa
#   Quindi nel codice 
#       self.colore = colore
#   creamo la proprietà colore e gli assegniamo il parametro colore 
#   che viene passato a __init__() in fase di creazione della classe

#   La confusione può sorgere dal fatto che sia la proprietà della classe 
#   che il parametro con cui viene inizializzata hanno lo stesso nome;

#   Questa però è una convenzione, e una volta passata la confusione è molto utile:
#   in questo modo uno capisce a colpo d'occhio che ci sono delle proprietà della classe
#   e queste proprietà vengono inizializa con __init__() ed hanno perciò 
#   lo stesso nome dei parametri del costruttore (__init__)

#   In questo modo se io vedo __init__(self, colore) saprò che nella classe 
#   probabilmente c'è una proprietà colore che io voglio che venga definita all'inizializzazione

# - __init__() è un metodo speciale, solo perchè viene chiamato quando la classe è instanziata in un oggetto;
#   ma comumque il costruttore si può chiamare di nuovo quando vogliamo, "a mano",
#   come chiamiamo per esempio fa_verso() quando e come vogliamo

paperotto.__init__("nero", "uff uff!")
print(paperotto.colore)
paperotto.fa_verso()







