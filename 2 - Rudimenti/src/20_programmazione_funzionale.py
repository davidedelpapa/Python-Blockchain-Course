# Programmazione Funzionale (FP)

# DISCLAIMER:
# Questa lezione è molto teorica, ma può essere
# molto soddisfacente maneggiare questi concetti.


# Ricordiamo per prima cosa che c'è un modo per intendere le funzioni (anche imformatiche)
# come delle operazioni che agiscono su un dominio con risultati in un altro dominio.

# Cos'è un dominio? In pratica un insimeme.
# Per esempio, la somma di numeri interi positivi (per semplicità usiamo quella)
#   A + B = X.
# A e B stanno nel dominio dei numeri interi, ovvero nell'insieme dei numeri interi
# N+ = {0, 1, 2, 3}
# X, il risultato, sta anche nel dominio dei numeri interi.

# Ciò però non vale per altre operazioni, per esempio la sottrazione, 
#   A - B = X.
# perchè se anche limitiamo A e B a N+ il risultato X piuò apparire fuori da N+, 
# per esempio 3 - 5 = -2
# Quindi anche se nella sottrazione A e B sono limitati a N+, il risultato X deve stare 
# in un altro insieme, N:
# N = {... -3, -2, -1, 0, 1, 2, 3, ...}

# Ripassiamo la notazione funzionale:

# f(x) -> y

# dove x sono le variabili in entrata e y quelle in uscita

# Abbiamo visto come x può stare in un dominio e y in un altro.
# Se vogliamo f(x) diventa una relazione, dove ad ogni "punto" 
# di un insieme corrisponde un punto in un altro insieme.
# Vediamolo con un esempio, dove x ha una sola variabile.
# Se mettiamo 
# f(x) -> y , dove f(x) è x-2

# in Python:
def f(x):
    return x - 2
    
# e iniziamo ad applicare in un loop di 5 elementi
for x in range(0, 5):
    print(f(x))

# vediamo il comportamento seguente:

# dominio      dominio
#    x            y
#   [0]--f(x)-->[-2]
#   [1]--f(x)-->[-1]
#   [1]--f(x)-->[ 0]
#   [3]--f(x)-->[ 1]
#   [4]--f(x)-->[ 2]

# Per ogni punto dell'insimeme x f(x) crea una 
# relazione univoca con un punto dell'insimeme y

# E se complicassimo un po' le cose aggiungendo
# un'altra funzione che usi il dominio y in entrata 
# e restituisca valori in un altro dominio?
# g(y) -> z
# Vediamolo con un esempio.

# Se mettiamo 
# g(y) -> z , dove g(y) è y+3

def g(y):
    return y + 3

# In python possimo direttamnete applicare g() a f()
# Iniziamo ad applicare in un loop di 5 elementi
for x in range(0, 5):
    print(g(f(x)))

# vediamo il comportamento seguente:

# dominio      dominio     dominio
#    x            y           z
#   [0]--f(x)-->[-2]--g(y)-->[1]
#   [1]--f(x)-->[-1]--g(y)-->[2]
#   [1]--f(x)-->[ 0]--g(y)-->[3]
#   [3]--f(x)-->[ 1]--g(y)-->[4]
#   [4]--f(x)-->[ 2]--g(y)-->[5]

# Fino ad adesso non siamo ancora entrati nel reame 
# della programmazione funzionale (FP) però, perchè avevamo 
# bisogno delle basi (lo so una introduzione lunghetta...)

# LAMBDA

# Entriamo nel vivo!
# Una lambda (carattere greco della l)
# è una funzione speciale anche in python: può essere usata per due motivi:
# 1. per indicare una funzione da usare una volta sola 
#    (quindi non c'è bisogno della sintassi classica),
# 2. in programmazione funzionale (yay!!)

# Sintassi:

y = lambda x: x - 2

print(y(3))

# Come si può subito notare, va assegnata ad una variabile, che sarà una "funzione"
# Di qui il suo carattere di funzione "usa e getta"
# In effetti, non c'è neanche bisogno di assegnare la lambda ad una varibile
# se abbiamo tutta l'intenzione di usarla una volta sola ("usa e getta", appunto)

# Ma andiamo con ordine. Infatti... dove meglio di un insieme possiamo usare una lambda?

# Entra in scena un'altra funzione basilare della FP, map()

# map() mappa una funzione su un dominio, 
# ovvero genera quella relazione che dicevamo di f(x)->y

y = lambda x: x - 2
x = range(0,5)
print(list(map(y, x)))

# ovviamente (ma non troppo) dovevamo trasformare il risultato di map() in una lista.
# Infatti scrivere solo
print((map(y, x)))
# informa solo che map è un oggetto e la sua posizione di memoria,
# ma non calcola i risultati

# E quindi che c'entrano le funzioni "usa e getta"?
# Possiamo riscrivere l'esempio del primo dominio come:

print(list(map(lambda x: x - 2, range(0,5))))

# dove notiamo che abbiamo sostituito x e y con la corrispettiva lambda e funzione

# Riscrivendo gli esempi finora:

y = lambda x: x - 2
z = lambda y: y + 3
x = range(0,5)
print( list(map( z , list(map( y, x )) )) )

# o, in maniera meno criptica:

y = lambda x: x - 2
z = lambda y: y + 3

# adesso creamo espressamente i domini di ritorno delle funzioni

dominioX = list(range(0,5))
dominioY = list(map(y, dominioX))
dominioZ = list(map(z, dominioY))

# e stampiamo a schermo i domini

print(dominioX)
print(dominioY)
print(dominioZ)

# Possimao rndere anche il tutto, moooolto criptico, usando
# la proprietà "usa e getta" delle lambda:

print(list(map(lambda y: y + 3, list(map(lambda x: x - 2, range(0,5))))))

# Certo, tutto in una linea di codice, ma molto criptica.
# Anche per gente navigata nell'FP è di difficile lettura!

# Qualcuno potrebbe obbiettare sull'utilità di usare l'FP,
# considerando che, se uno non ci è abituato, sono un sacco di parentesi e
# c'è molto da interpretare. Tutto questo è vero.
# Quindi ovviamente, se si usa la FP ci sono dei vantaggi tangibili.

# Nei calcoli matematici ci sono notevoli vantaggi, ma non si riducono ai calcoli matematici.
# Per vederlo dovremmo buttarci fittamente nel mondo della FP.
# Comunque basti pensare che molte delle nuove tecnologie, 
# in primis quelle sull'intelligenza artificiale, fanno ricorso alla FP, 
# anche solo per semplificare i calcoli.

# Come introduzione penso questo sia sufficiente.


