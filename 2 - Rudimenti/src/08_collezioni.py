# Collezioni

# Sono insiemi di oggetti più semplici, ognuno con le sue caratteristiche specifiche

# Vedremo di seguito:
# - liste       Una collezione ordinata e mutabile
# - tuple       Una collezione ordinata e immutabile
# - set         Una collezione non ordinata e senza indici, mutabile
# - dizionari   Una collezione non ordinata, con indici, mutabile

# Vedremo per ognuna cosa significano le caratteristiche espresse.
# Si noti come le tuple siano le uniche collezioni immutabili
