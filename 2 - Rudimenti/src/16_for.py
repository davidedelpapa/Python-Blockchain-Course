# For

# Il for serve principalmente per applicare blocchi di codice agli elementi di una collezione
# Di per sè quindi più un costrutto funzionale che legato al controllo dell'esecuzione

# Forma:

# for <variabile_temporanea> in <collezione>:
#   <ciclo_da_eseguire>

# Si noti la presenza di una variabile temporanea:
# di volta in volta l'elemento della collezione che si sta valutando
# sarà presente ed accessibile nella variabile temporanea

# Ovviamente possiamo chiamare la variabile temporanea come vogliamo,
# ma un nome esplicativo servirà meglio.


# Esempi:

# Liste

lista = ["Sole", "Mercurio", "Venere", "Terra"]

for x in lista:
    print("Stiamo considerando: " + x)

# Tuple

tupla = (1, 2, 3, 2)

for elemento in tupla:
    print(elemento + 2)

# Set (Insiemi)

insieme = {"Carlo", 1, "Giovanni", 2}

for el in insieme:
    print(str(el))  # Ricordarsi del casting!
                    # Ricordarsi anche che l'insieme non è ordinato!

# Facciamo un ripasso anche dei dizionari:
                 
dizionario  = {1: "Alessandro", 2: "Carlo", 3: "Davide", 4: "Marino", 5: "Simone"}                 

for chiave in dizionario:
    print(chiave) # stamperà solo le chiavi

for chiave in dizionario:
    print(dizionario[chiave]) # stamperà il valore corrispondente alla chiave

for chiave, valore in dizionario.items():
    print(str(chiave) + ": " + valore) # Adesso abbiamo a disposizione sia la chiave sia il valore

# Anche le stringhe sono una collezione di caratteri

stringa = "Ciao"

for carattere in stringa:
    print(carattere)

# Ciclo sul range()

# La funzione range() genera una sequenza numerica iterabile

# Con la funzione range abbiamo la possibilità di 
# trasformare il for in un ciclo più propiamente detto.

# Esempi:

for x in range(6):
    print("ciclo n° " + str(x)) # Ricordarsi!
                                # Da dove si inizia a contare?
                                # L'estremo escluso!
                                # Casting!

for x in range(2, 7):
    print("ciclo n° " + str(x)) # Inizia da 2, ma fin dove arriverà?


# Anche per il ciclo for ci sarebbe molto altro da dire,
# ma questo è un corso basico, quindi non possiamo trattare tutto.








