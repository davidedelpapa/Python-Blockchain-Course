# Ci sono 3 tipi di numeri in Python

# 1. int:       interi
# 2. float:     a virgola mobile, in breve i numeri reali
# 3. complex:   complessi

# Esempi:

x = 13 
y = 3.1416 # Meschina approssimazione di pi-greco
z = 1j #j è usato al posto del i in questo caso, per rendere i numeri complessi

# Per verificare il tipo di una variabile si usa la funzione type()

print(type(x))
print(type(y))
print(type(z))

# INT -- Gli interi

# Positivi o negativi

x = 15
y = 32467478990546849365
z = -4275642
print(type(x))
print(type(y))
print(type(z))

# Una caratteristica che rende python speciale:
# Gli interi possono essere di lunghezza arbitraria
# --> Non è scontato tra i linguaggi di programmazione
# --> Ovviamente l'interprete ci mette più tempo per fare i calcoli con grandi numeri


# FLOAT -- I numeri reali (a virgola mobile)

# Positivi o negativi; contengono decimali

x = 0.0 # alternativo x = .0
y = -0.1
z = 567.890123

print(type(x))
print(type(y))
print(type(z))

# Nomenclatura scientifica

# Dove e oppure E sono potenze di 10

x = 32e7
y = -52.9E3
z = 156e-5

print(type(x))
print(type(y))
print(type(z))

# Proviamo a vedere che sono?
print(x)
print(y)
print(z)

# COMPLEX -- I numeri complessi

# Si scrivono usando j invece di i nella parte immaginaria

x = 7-2j
y = 8j
z = -4j

print(type(x))
print(type(y))
print(type(z)) 

# Da notare che quando ci può essere confusione 
# il python stampa il numero complesso fra parentesi.

print(x) # 7-2j potrebbe sembrare l'intero 7 meno 2j...
print(y) # 8j non genera confusione
print(z) # Qui il python converte il numero in (-0-4j) onde evitare confusioni



