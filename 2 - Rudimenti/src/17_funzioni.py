# Funzioni

# Ci sono due modi per vedere le funzioni in informatica

# 1° modo (per chi è aduso alla matematica!)

# f(x) -> y

# In questo caso pensiamo alle funzioni in informatica come a quelle in matematica,
# ovvero un'operazione (o serie di operazioni) da applicare ad un dominio 
# ed ha risultati in un altro dominio.

# Ma questa visione che è molto in voga in alcuni circoli è molto fuorviante
# Per chi non conosce questo aspetto matematico non importa.

0
# 2° modo (visione più informatico-pratica)

# Pensiamo alle funzioni come blocchi di codice 
# che accettano variabili in entrata 
# e producono variabili in uscita

# In entrambi i casi, sia le variabili in entrata che quelle in uscita possono essere omesse
# Ragion per cui le funzioni del python hanno poco a che vedere con le funzioni matematiche.

# Ma bando alle ciancie, passiamo ai fatti:

# Forma:

# def <nome_funzione>(<parametri>):
#   <codice_da_eseguire>

# Esempi:

def funzione ():
    print("Ciao a tutti, sono un funzione!")

# La funzione va poi chiamata.
# ne abbiamo chiamate tante sensa saperlo, basta mettere il nome, 
# le parentesi e all'interno le variabili da passare (se necessarie)

funzione() # appena chiamata! Il codice verrà subito eseguito.

# print() è una funzione, len() è una funzione, range() è una funzione.
# Mentre invece i metodi degli oggetti, 
# tipo stringa.lower() non sono funzioni in questa ottica, 
# perchè sono legate agli oggetti


# Parametri

# Perchè una funzione accetti parametri basta dichiararli separti da virgole 
# all'interno della parentesi nella definizione della funzione

# Esempi:

def funzione_parametrica(parametro1, parametro2):
    print(parametro1)
    print(parametro2)

funzione_parametrica(1, 2)
funzione_parametrica("Pasquale", "Giovanni")
funzione_parametrica(1, "Giovanni")

# Valori di ritorno

# Si usa la parola chiave "return" per stabilire dei valori di ritorno della funzione

# Esempi:

def somma_due(x):
    r = x + 2
    return r

print(somma_due(3))

# possiamo anche mettere l'ultima espressione nel ritorno direttamente

def esp_due(x):
    return x ** 2 # l'operatore ** eleva x alla potenza di 2

print(esp_due(9))

# Ovviamente si può assegnare la variabile di ritorno ad unb'altra variabile.

def dammi_cinque():
    return 5

x = dammi_cinque()
print(x + 2)


# Parametri di default

# Si possono anche stabilire parametri di default alla creazione delle funzioni.
# Basta assegnare un valore al parametro quando viene definita la funzione

def esponenziale (base, potenza = 2):
    return base ** potenza

# Se il secondo parametro non viene posto, si usa di default il valore assegnato.

print(esponenziale(2,3))
print(esponenziale(3))

# Come sempre ci sarebbero molte altre cose da dire sulle funzioni, 
# alcune delle quali verranno affrontate nella lezione sulla programmazione funzionale.


