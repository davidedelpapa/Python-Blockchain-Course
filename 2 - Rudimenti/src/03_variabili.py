# Una variabile serve per incamerare dati

# In python una variabile è creata
# nel momento in cui le si assegna un valore

x = 5  # assegnamo il valore 5 ad x
y = 7  # assegnamo il valore 7 ad y
z = x + y  # Che valore avrà mai z?

# Le variabili sono, appunto, variabili

x = 1
x = 2
x = 3  # stiamo riassegnando sempre valori ad x
x = "Ciao!" # adesso x contiene una stringa, non un numero!

# A che serve tutto ciò?

# A S T R A Z I O N E !!!

x = input("Come ti chiami? ")
print("Ciao, " + x)

# Nota: se input non funziona state usando python 2!!!!
#       Installare il 3, prego!
# Per controllare la versione:

import sys  # ignorare per ora
print(sys.version)

# Regole per i nomi
# -----------------

# 1 - deve iniziare per lettera o underscore (_)

prima_variabile = 1 # ok

_2_variabile = 2 # ok, ma ha un "significato" speciale che vedremo più avanti

# 3a_variabile = 3 # ERRORE! una variabile non può iniziare per numero

# -----------------------------------------------------------

# 2 - Può contenere solo codici alfanumerici e underscore (_)


variabile1 = 2 # ok
# valore_in_$ = 3.2 # ERRORE! il simbolo $ non è alfanumerico!

# -----------------------------------------------------------

# 3 - Le variabili sono "case sensitive" ovvero maiuscole e minuscole non sono la stessa cosa

Giovanni = 1
giovanni = 2
print(Giovanni)
print(giovanni)

# BISOGNA FARE ATTENZIONE!!!

# Concatenare variabili

X = "Ciao"
Y = "Mondo"
Z = X + ", " + Y 

print(Z + "!")

# Occhio però a non concatenare stringhe e numeri

x = 1
y = "Giovanni"
# print(x+y) # ERRORE!

# Vedremo nella lezione 6 sul casting come risolvere questo problema!!!

