# If

# Ovvero, if...elif...else


# Per prendere decisioni rispetto a condizioni il costrutto principe in informatica è l'if
# In inglese "if" vuol dire "se", intendendo "se è vera questa condizione, allora esegui questo blocco"

# Forma:

# if <condizione_vera>:
#    <blocco_da_eseguire>

# Si noti la presenza del due punti (:) alla fine della condizione,
# e dell'indentazione del blocco da eseguire.

# La condizione è un'espressione che consiste di solito in vari operatori.
# Se la condizone è vera il blocco che ne segue verrà eseguito, 
# altrimenti si procede oltre ignorando il blocco.

# Esempi:

a = 2
x = "vero"

if a <= 3:
    print("a minore o uguale a 3")
    print("questo blocco verrà eseguito")

if a > 10:
    print("è impossibile che a, che vale 2, sia maggiore di 10")
    print("questo blocco non verrà eseguito")

if x == "falso":
    print("questo blocco non verrà eseguito")

# Possiamo complicare di più le condizioni, volendo

if x == "vero" and a != 3:
    print("questo blocco verrà eseguito")   # abbiamo messo un'espressione complessa
                                            # con vari operatori

if not x != "vero":
    print("questo blocco verrà eseguito o no???") # Per scoprirlo, ricordarsi il significato di not

# Ovviamente possiamo anche testare fra di loro due variabili

a = 5
b = 7

if a > b:
    print("questo blocco non verrà eseguito")


# Elif

# Per testare successivamente varie condizioni, si può usare elif,
# che serve a dare una nuova condizione se la precedente non si è verificata.

a = 5
b = 7

if a > b:
    print("questo blocco non verrà eseguito")
elif a > b:
    print("Adesso sì")
    print("questo blocco verrà eseguito")


# Else

# Per chiudere un blocco di condizioni, talvolta è necessario prendere azioni 
# quando nessuna delle condizioni precedenti si sia verificata
# In questo caso si usa else.

a = 5
b = 7

if a > b:
    print("questo blocco non verrà eseguito")
elif a == b:
    print("neanche questo blocco verrà eseguito")
else:
    print("visto che nessuna delle condizioni si è verificata...")
    print("questo blocco verrà eseguito")

# I costrutti dell' if sono ovviamente utilissimi in programmazione e molto diffusi.
# Per queste ragioni è buona cosa sapere bene e a fondo questo argomento.


