# While

# Il while serve per eseguire un ciclo fintantochè una condizione resta vera

# Forma:

# while <condizione_vera>:
#   <ciclo_da_eseguire>

# Esempi

x = 1

while x <= 10:
    print("Ciclo n° " + x)
    x += 1  # Ricordate? 
            # Significa x = x + 1
            # Stiamo incrementando x di 1 ad ogni ciclo

# E' importante accertarsi che ci sia una condizione per cui il ciclo termini, 
# altrimenti non terminerà mai e si dovrà procedere a bloccare di forza il programma

x = 1

while x > -10:
    print("ciclo matto")
    # x+=1  #con questo loop la condizione sarà sempre vera e il ciclo continuerà sempre
    x -=1   # con questo il ciclo avrà un termine

# Ci sono altri argomenti legati al ciclo while, come interruzione e ripresa dei cicli, 
# ma andranno lasciati ad un altro corso più dettagliato.


