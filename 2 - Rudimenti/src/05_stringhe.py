# Le Stringhe

# Sono una serie di caratteri rinchiusi fra virgolette semplici (') o doppie (")
# Rappresentano del testo

x = "Ciao"
y = "Giovanni è amico mio"
z = """Si può usare una docstring anche
ma fate attenzione, se ci sono spazi faranno parte della variabile"""

print(type(x))
print(type(y))
print(type(z))

print(x)
print(y)
print(z)

# Unicode
# in python 3 le stringhe letterali sono unicode

str_unicode = "😀😀😀"

print(str_unicode) # Nella console qualche volta l'uncode lascia a desiderare
print(type(str_unicode)) # è una str come le altre...

# Operazioni su stringa

# Ci portiamo un po' avanti col lavoro, ma una stringa cos'è?
# In python una stringa è semplicemente una LISTA DI CARATTERI
# Questo ha il vantaggio che si può "giocare" con i testi facilmente.

# Per esempio, se vogliamo ottenere la lunghezza della stringa:

s = "Se vuoi sapere quanti caratteri ci sono devi contare!!!"
print(len(s))

# Gli spazi contano come un carattere; di fatto nella lista della stringa s ci sono contati i caratteri vuoti (spazi)

s = "   "
print(len(s))

# len infatti conta quanti elementi ci sono nella lista
# Possiamo pure assegnarlo ad una variabile

s = "Ciao, mondo!"
l = len(s)
print(type(s))
print(type(l))
print(l)

# Se vogliamo ottenere un carattere specifico, dobbiamo usare la funzione posizionale di una lista

s = "Ciao, mondo!"
print(s[2]) # Qual'è la posizione 2?
            # Di solito in informatica il primo di una lista è l'elemento [0]
            # e il python non fa eccezine in questo!

# Cosa fondamentale per capire le posizioni ordinali 
# nella maggior parte dei linguaggi di programmazione.
# SI INIZIA A CONTARE DA 0!!!


# Possiamo prendere pure ottenere sottostringhe (substring)

print(s[2:7]) # L'ultima esclusa

# E' molto importante capire questo: dalla posizione 2 alla posizione 6, l'ultima è esclusa!

# Se si omette un estremo, invece, si considera tutto a partire da quell'estremo

print(s[:2]) # Cosa stamperà?
                
# Aiutino: l'ultima esclusa!

print(s[9:]) # Cosa stamperà?

# Aiutino: si inizia a contare da 0!

# Strip, ovvero, rimuovere spazi bianchi, all'inizio o alla fine.

s = "    Ciao, mondo!       "
print(s.strip())
print(s)

# Vediamo qui per la prima volta un formato strano: strip non è come una funzione!
# Finora, tutte le funzioni che abbiamo visto seguivano la forma

#   f(x)

# esattamente come una funzione matematica
# Strip invece funziona diversamente.

# Strip è un metodo intrinseco di ogni stringa.
# La differenza è semplice: mentre la funzione 
#   f(x) -> y 
# si applica su x e da come risultato y,
# un metodo è per così dire un'azione che può fare un ogggetto.

# Il metodo strip è un'azione dell'oggetto stringa che ha un risultato dell'azione.

# Ai fini pratici può sembrare la stessa cosa, ma mentre una funzione è una cosa esterna all'oggetto, 
# l'azione è propria dell'oggetto stesso.

# L'oggetto stringa ha anche altri metodi:

# Lower e upper

s = "Ciao, mondo!"
print(s.lower())
print(s.upper())

# Replace fa sostituzioni di caratteri

s = "Ciao, mondo!"
print(s.replace('m', 'f'))

# Mentre a split si deve passare un separatore perchè crei delle sottostringhe
s = "Ciao, mondo!"
print(s.split(','))

# Split diventa molto utile per maneggiare file csv
# ovvero tavole (tipo excel) dove i dati sono separati da virgole
# Vedremo più in là esempi e modi per comporre tutto quello che stiamo imparando ora.



