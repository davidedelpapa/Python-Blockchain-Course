# Operatori

# Un operatore serve per fare operazioni fra variabili e/o valori
# Il python ha molti operatori, ne vedremo solo un po' rimandando lo studio specifico ad altri corsi più approfonditi.


# Operatori matematici

# Questi sono facili + * - / per le 4 operazioni matematiche

x = 3
y = 2
print(x + y)
print(x - 1)
print(5 * 4)
print(x / y)

# Come vedete si possono fare operazioni su variabili, su valori, o mischiare le due cose.


# Operatori di Assegnamento (chiamato talvolta anche assegnazione)

# Questo già l'abbiamo visto nella forma semplice fin dall'inizio,
# ma non sapevamo come si chamasse.

x = 1 # Assegnamento
y = x # Un altro assegnamento

# Altri tii di assegnamento prevedono anche operazioni durante l'assegnamento
# ESEMPI

x = 1
x += 2 # E' equivalente a x = x + 2, ovvero maggiorare la x di 2
print(x)
x -= 1 # CHe valore avrà adesso?
x *= 3 # Ed ora?
x /= 2 # Sate seguendo?
print(x)

# Esatto: un float! 
# Anche se il risultato è un numero intero, la divisione fra interi genera float di default.


# Operatori di Comparazione

# Gli operatori di comparazione risultano in espressioni vere o false

x = 2
print(x == 2)


# NOTA BENE

# Come vedete ci potrebbe essere confusione all'inizio fra assegnamento e uguaglianza
# L'uguaglianza è anch'essa un operatore, ma viene espressa con due (2) simboli di uguale (==)

# C'è anche l'inverso, la diseguaglianza

x = 2
print(x != 3)

# Altri operatori di comparazione

print(3 > 2)
print(5 < 3)
print(2 > 2)  # Un numero non può essere maggiore di sè stesso
print(2 >= 2) # ...ma può essere maggiore o uguale a sè stesso
print(2 <= 5)

# Nota i messaggi che python ci dice, True e False (vero o falso in inglese), non sono in realtà messaggi

x = 2 == 3

# Qui abbiamo assegnato il risultato della comparazione alla variabile x
# Ma cos'è x?
# Forse una stringa con su scritto vero o falso (True/False)?

print(type(x))ù

# bool???
# E che è?

# bool o booleano (dal celebre matematico G. Boole) 
# è un tipo di variabile cha ha solo due valori: vero o falso!

# Ci occuperemo di queste variabili più in là.


# Operatori Logici
# Eseguono operazioni logiche (logica booleana) e quindi hanno come ritorno un valore bool

# and

x = 2
print(x == 2 and x < 3)

# And dà vero solo se entrambe le espressioni sono vere

print(x == 2 and x > 3) # Falso!

# or

# Or dà vero se almeno uno dei due è vero

x = 2
print(x == 2 or x >= 3) # vero(True), perchè x == 2, anche se non è maggiore o uguale a 3

# not

# Dà l'inverso del risultato

x = 2
not(x == 2)

# NOTA BENE

# E' bene talvolta anche esagerare con le parentesi in queste espressioni, 
# anche se non ci andrebbero o si ritiene che non ci andrebbero parentesi,
# per rendere più esplicito cosa si voglia verificare!

# Esempio

x = 2
print(not x == 2 or x < 3) # True
print(not (x == 2 or x < 3) ) # False

# Nel primo caso facciamo l'inversa solo della prima espressione, 
# nel secondo facciamo l'inverso di entrambe le espressioni dell'or.

# Per verificare cosa si stia facendo si potrebbe procedere per parti:
x = 2
print(not x == 2)
print(x == 2 or x < 3)
print(not x == 2 or x < 3)

# Se vi viene il mal di testa non preoccupatevi, sono cose che si imparano facendole!
