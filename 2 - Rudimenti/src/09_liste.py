# Liste
# Una collezione ordinata e mutabile

# Forma:

lista = ["uno", "due", "tre", "quattro", "cinque"]
print(lista)

# Come abbiamo visto con le stringhe (che sono particolari liste)
# possiamo accedere ad ogni elemento della lista.

print(lista[1]) # Si inizia a contare da...?

# Questo aspetto che la lista è accessibile per elemento la rende ORDINATA.

# Si applicano per le liste anche alcuni altri aspetti che abbiamo visto per le stringhe,
# mentre altri non si applicano.

print(lista[1:3])
print(lista[:3]) # Ricordate ciò che abbiamo detto sull'estremo escluso!
print(lista[3:])

# print(lista.upper()) # ERRORE! Non possiamo applicare proprio tutto, non tutto ha un senso!

print(len(lista)) # Questo si applica eccome!

# Ma la lista ha anche altri metodi suoi!

# Cambiare un elemento della lista

lista[2] = "otto"
print(lista)

# Una volta creata la lista per aggiungere elementi si usa il metodo append()

lista = ["uno", "due", "tre", "quattro", "cinque"]
lista.append("sei")
print(lista)

# Mentre per inserire in una specifica posizione si usa il metodo insert()

lista = ["mela", "pera", "banana", "albicocca"]
lista.insert(2, "pesca") # Da dove si inizia a contare?
print(lista)

# Per rimuovere elementi abbiamo vari metodi

lista = ["mela", "pera", "pesca", "banana", "albicocca"]
lista.pop(2) # Da dove si inizia a contare?
print(lista)

lista.pop() # Senza una posizione specificata, 
            # pop() diviene il contrario di append()
            # rimuove l'ultimo elemento
print(lista)

# La funzione remove()'

lista = ["mela", "pera", "pesca", "banana", "albicocca"]
lista.remove("pera")
print(lista)

# NOTA BENE
# Occhio alla funzione remove()!
# All'inizio ci otrebbe essere confusione e tentare di usarla posizionalmente

lista = ["mela", "pera", "pesca", "banana", "albicocca"]
# lista.remove(1) # ERRORE! L'elemento 1 non è presente

# Ma una lista non deve per forza contenere stringhe!

lista = [4, 3, 2, 1]
lista.remove(1) # Tutto OK, ma cosa ha rimosso?
print(lista)    # remove ha rimosso l'elemento 1, non l'elemento in posizione 1!!!

lista = [4, 3, 2, 1]
lista.pop(1)    # invece...
print(lista)

# Quindi molta attenzione ad usare pop() e remove() evitando confusioni


# Un ultima nota:

lista = [False, "uno", 2, "tre", .4, "cinque"]
print(lista)

# Una lista può contenre multipli tipi di oggetti.


# Ci sono moltissime altre cose da dire sulle liste, ma per questo rimandiamo a corsi più specializzati


