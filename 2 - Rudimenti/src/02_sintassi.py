# Il python  e la sua sintassi
# Il python HA la sua sintassi !!!!

# Questo è un commento
# Tutti i commenti iniziano per #
# il resto della riga viene considerato un commento

print("Questo non è un commento") # ma questa parte della riga sì!

# Il python usa anche l'indentazione in modo formale.
# Questo vuol dire che gli spazi bianchi ad inizio riga 
# non sono decorativi come in altri linguaggi.

# Esempio. Non badate a cosa vuol dire, 
# anche se il Python è molto semplice da capire
if 2 > 1 :
    print("2 è maggiore di 1") # Nota bene lo spazio prima del print

# altro esempio
if 2 < 1:
    print("Questo non verrà mai stampato a video") # 2 non sarà mai minore di 1
    print("Neanche questo.")    # qui, vista l'indentazione 
                                # siamo ancora ll'interno dellecose da fare 
                                # se la condizione fosse vera

print("Questo sì che viene stampato")   # qui non c'è nessuna indentazione, 
                                        # il programma riprende normalmente.

# NOTA BENE
# Per indentare si possono usare sia spazi bianchi che tablature (tasto tab)
# Però non si possono mai mischiare nello stesso blocco.
# Dato che a schermo non si vedono, a meno di essere evidenziati i caratteri,
# è opportuno scegliere uno stile ed usare sempre quello.

# La maggior parte dei programmatori odierni utilizza 4 spazi bianchi per indentare il codice


# Commenti "docstring"
# Iniziano e terminano con 3 simboli " oppure '

""" Questo è un docstring di una riga sola """

# Sono particolari tipi di commenti che servono a documentare il codice
# Hanno la particolarità di spaziare su più righe, se serve.

"""
multiline
docstring
"""

''' Anche questo 
è un docstring '''

# E' buona prassi usare docstring per documentare il codice.

""" Il codice all'interno dell'if-block non verrà mai eseguito """
if 5 > 10:
    print("Impossibile!")
