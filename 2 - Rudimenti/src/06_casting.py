# Il casting serve per passare da un tipo di dato ad un altro.

# E' una manira di forzare "intelligentemente" un tipo affinchè si converta in un altro.
# Ovviamente ci sono alcune regole che si applicano.

# Vediamo alcuni esempi.


# int()

# Trasforma l'oggetto in numero intero.

x = int(.3)
print(type(x))
print(x)

# Per i float int() tronca la parte a virgola mobile

x = int(17.99)
print(x)

# Se vogliamo arrotondare invece di troncare bisogna usare la funzione round()

x = round(17.99)
y = round(15.32)
z = round(3.5) # Nota bene!

print(x)
print(y)
print(z)

# E una stringa?

x = "12"
y = int(x)

print(type(x))
print(type(y))

print(x)
print(y)

# E se è una stringa che non ha nulla a che vedere con i numeri?

x = "Giovanni"
# print(int(x)) # ERRORE! No si può fare il casting in queste condizioni

# Questa è una cosa utile da sapersi.
# La tentazione di usare il casting solo per risolvere problemi è molto forte a volte.

# Per esempio nel richiedere informazioni agli utenti

x = input("Quanti anni hai? ")
y = int(x) # Possibile fonte di guai immensi
print(y)

# Cosa succede se l'utente sbaglia a premere un tasto o immette un testo invece di numeri?

x = input("Quanti anni hai? ") # L'utente risponde "dodici"

# Vedremo in seguito modi di ovviare a questo, anche tramite la gestione degli errori.



# float()

# Trasforma l'oggetto in numero reale

x = 1
y = "32.5"
z = "0"
print(float(x))
print(float(y))
print(float(z))

# Per il resto valgono le regole che abbiamo visto con int()


# str()

# Trasforma l'oggetto in stringa

x = str(1)
y = str(.5) # Qui avviene una conversione dietro le quinte che vedremo poi

print(type(x))
print(type(x))

print(x)
print(y) # E' l'assegnazione stessa che lo ha convertito in forma canonica


# Vi ricordate l'errore che avevamo nella lezione 3 sulle variabili?
x = 1
y = "Giovanni"
# print(x+y) # ERRORE!

# Come si sommano se una è stringa e l'altra è numero?
# Col casting, ovviamente!

print(str(x)+y)


