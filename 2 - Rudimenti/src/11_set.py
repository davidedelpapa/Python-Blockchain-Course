# Set o Insieme
# Una collezione non ordinata e senza indici, mutabile

# In realtà in italiano set si traduce come insieme, 
# ma è invalso l'uso di lasciare anche il nome come in inglese

# Forma:

insieme = {"Sole", "Mercurio", "Venere", "Terra"}

# Perchè non ordinata?

insieme = {"Sole", "Mercurio", "Venere", "Terra", "Marte", "Giove"}
print(insieme)

# L'interprete python ha la facoltà di immagazzinare un insieme esattamente nell'ordine che vuole

# Non essendoci un ordine, non c'è un metodo del set preposto per l'accesso.
# Come si accede allora agli elementi del set?

# Si può utilizzare il ciclo for, che studieremo più avanti

insieme = {"Sole", "Mercurio", "Venere", "Terra"}
for x in insieme:
    print(x) # Ricordate la lezione sulla sintassi e l'indentazione?

# Da notare come i 2 punti siano obbligatori così come l'indentazione
    
insieme = {1, 2, 3, 4, 5}
for x in insieme:
    print(x)
    print(x <= 3) # Stessa indentazione come la riga sopra = parte dello stesso blocco

# Possiamo anche controllare se un elemento è in un insieme

insieme = {"Sole", "Mercurio", "Venere", "Terra", "Marte", "Giove"}
print("Urano" in insieme)
print("Urano" not in insieme)

# Modificare lementi di un set

# ERRORE! Non si può fare!
# Ma non era mutabile?

# Si può aggiungere un elemento al set
insieme = {"Sole", "Mercurio", "Venere", "Terra", "Marte", "Giove"}
insieme.add("Saturno")
print(insieme)

# O toglierne uno
insieme = {"uno", False, 2.3}
insieme.remove(2.3)
print(insieme)

# NOTA BENE
# Non si possono ripetere elementi di un insieme; se ripetuti contano per uno

insieme = {"uno", False, 2.3, False, True}
print(len(insieme)) # Ovviamente si possono contare quanti elementi ci sono
print(insieme)



