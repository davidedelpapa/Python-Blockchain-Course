# Eccezioni

# Molte volte si deve poter gestire gli eventuali errori che si presentano 
# senza dover interrompere il flusso di un programma.


# Try...except

# try permette di eseguire un blocco di codice che potrebbe generare errori,
# in modo da poterli gestire.
# except è la maniera di gestire tali errori.

try:
    print(x) # Errore! x non è stato definito in antecedenza!
except:
    print("Si è verificato un errore!")
    
# Si possono catturare anche errori specifici

# per esempio:

# print(12/0) genera un errore chiamato ZeroDivisionError
# ovvero errore di divisione per 0
# Proviamo ad intercettare tale errore

try:
    print(12/0)
except ZeroDivisionError:
    print("Errore: non si può dividere per 0!")

# Si possono mettere vari except in serie, per beccare vari possibili errori
# Da ultimo si può mettere anche un except generico (senza specificare il tipo di errore).
# In questo modo si possono gestire anche errori imrevisti.

try:
    print(12/0)
    print(x)
except NameError:
    # NameError è l'errore generato quando non viene definita una variabile
    print("Errore: x va definita")
except:
    print("Si è verificato un non meglio classificato errore.")
    
# NOTA BENE

# 1. Come si può constatare dall'esecuzione, il blocco si ferma al primo errore incontrato!
# 2. Si deve leggere la documentazione delle varie funzioni per capire che tipo di errore genereranno
#    in questo modo, sapendo in anticipo, tali errori si possono catturare più facilmente.

# Rimaneggiamo codice vecchio:

# Abbiamo visto come chiedere informazioni all'utente direttamente può essere fatale.
# Vediamo come rimaneggiare il tutto gestendo le eccezioni (errori)

nome = input("Come ti chiami? ")
print("Ciao, " + str(x)) # Da notare il casting a stringa
# Il casting a stringa è meno problematico

try:
    anni = input("Quanti anni hai? ")
    print(int(anni)) # può essere problematico!
except ValueError:
    # ValueError è l'errore relativo al casting impossibile
    print("Non è stato inserito un numero d'anni valido")



