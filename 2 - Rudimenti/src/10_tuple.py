# Tupla
# Una collezione ordinata ed immutabile

# Quella che in éyhton chiamiamo tupla, in realtà è molto più vicina 
# al concetto di ennupla matematica, per chi fosse in conoscenza di teoria matematica.

# Forma:

tupla = ("uno", "due", "tre")

# La tupla è ordinata, esattamente come una lista

tupla = ("uno", "due", "tre")
print(tupla[1])

# Ma a differenza delle liste è immutabile!

tupla = ("uno", "due", "tre")
# tupla[1] = "no!" # ERRORE! E' immutabile!

# Stesso dicasi per aggiungere elementi ad una tupla o rimuoverli!
# LE TUPLE SONO IMMUTABILI

# Ovviamente è possibile conoscere la dimensione di una tupla

tupla = ("uno", "due", "tre")
print(len(tupla))

# Ma a che servono le tuple se sono immutabili?
# Le tuple svolgono varie funzioni, che vedremo più in là


