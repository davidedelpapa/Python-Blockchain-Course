# Dizionari
# Una collezione non ordinata, con indici, mutabile

# Un dizionario è come un set di coppie chiave:valore
# Cosa vuol dire? E' come un set indicizzato, dove l'indice è una chiave.
# Il resto spiegherà bene cosa voglia dire tutto ciò.

# Forma:
dizionario = {"nome": "Giovanni", "cognome": "Rossi", "età" : 68}

# Per chiarezza molte volte si può trovare nella forma:

dizionario = {
    "nome": "Giovanni",
    "cognome": "Rossi",
    "età" : 68
}

print(dizionario)

# Il dizionario è accessibile tramite l'indicizzazione delle chiavi

print(dizionario["età"])

# Lo stesso è possibile tramite il metodo get()
print(dizionario.get("nome"))

# E' possibile cambiare il valore corrispondente ad una chiave, accedendo al dizionario per chiave

dizionario["cognome"] = "De Luca"
print(dizionario)

# Accedendo tramite for loop bisogna fare attenzione a ciò che si vuole stampare, se le chiavi o i valori.
# Il ciclo for infatti accede alle chiavi

for x in dizionario:
    print(x)

# Questo stamperà tutte le chiavi nel dizionario

# Notate la prossima forma per capire come accedere per valore:

for x in dizionario:
    print(dizionario[x])

# per ottenere contemporaneamente chiave e valore bisogna usare il metodo items() ["items" significa "elementi" in italiano]

for c, v in dizionario.items():
    print(c + ": " + str(v))

# Da notare il casting a stringa dei valori, utile per gestire i numeri

# Per controllare l'esistenza di una chiave usare il formato con in che abbiamo visto per i set

print("età" in dizionario) # True "età" è una delle chiavi
print("Giovanni" in dizionario) # False  "giovanni" è uno dei valori, non figura tra le chiavi

# E per cercare anche per valore?
# Più in là lo vedremo.

# Aggiungere un elemento ad un dizionario segue la stessa forma del mutare un elemento.
# Diciamo che la stessa forma modifica una chiave, o se la chiave non c'è, aggiunge una chiave nuva e le assegna un vaolre.

dizionario = {"nome": "Giovanni", "cognome": "Rossi", "età" : 68}
dizionario["età"] = 67          # Qui aggiorna il valore connesso alla chiave "età"
dizionario["città"]  = "Roma"   # Qui crea una nuova chiave, "città", assegnanogli il valore di "Roma"
print(dizionario)

# Per rimuovere un elemento usiamo il metodo pop() che già conosciamo bene
dizionario = {"nome": "Giovanni", "cognome": "Rossi", "età" : 68}
dizionario.pop("cognome")
print(dizionario)

# Come si può immaginare, i dizionari sono molto utili per immagazzinare informazione che sia in qualche modo strutturata
# Ne vedremo vari usi in seguito

