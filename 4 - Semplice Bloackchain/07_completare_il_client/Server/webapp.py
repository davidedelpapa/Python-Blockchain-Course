import hashlib
import json

from flask import Flask, jsonify, request
from Blockchain import Blockchain, hash

app = Flask(__name__)
chain = Blockchain()

@app.route('/')
def root_path():
    """ root_path: messaggio di benvenuto """
    url_ultimo_blocco = "{}last_block".format(request.base_url)
    url_movimento = "{}movimento".format(request.base_url)
    # Semplifichiamo i messaggi inutili
    return "Semplice Blockchain", 200

@app.route('/last_block')
def last_block():
    """ last_block: ottieni l'ultimo blocco """
    return jsonify(chain.last_block), 200

@app.route('/movimento', methods=['GET', 'POST'])
def set_transaction():
    """ registra un movimento """
    if request.method == "POST":
        recipient = request.form.get('recipient')
        sender = request.form.get('sender')
        amount = request.form.get('amount')
        return "{}".format(chain.add_transaction(sender, recipient, amount))
    else:
        return """Usa il metodo POST per registrare un movimento<br>
        Parametri: <br>
        sender:     da<br>
        recipient:  a<br>
        amount:     quanti soldi<br>
        """

@app.route('/ultimi_movimenti')
def last_transactions():
    """ ottienei la lista degli ultimi movimenti che verranno salvati nel prossimo blocco da minare... """
    return jsonify(chain.transactions), 200

@app.route('/mine', methods=['GET', 'POST'])
def miner():
    """ propone il nonce minato per l'ultimo blocco """
    if request.method == "POST":
        miner_nonce = request.form.get('nonce')
        miner_address = request.form.get('recipient')
        # Spostiamo l'onere della verifica nella classe Blockchain'
        if chain.verify_reward(miner_nonce, miner_address):           
            return "Transaction correct", 200
        return "Error: nonce not working", 403
    return """
            Usa il metodo POST per minare l'ultimo blocco<br>
            Parametri: <br>
            nonce:      il nonce corretto dell'ultimo blocco<br>
            recipient:  dove accreditare il coin di "compenso"
            <br>
            Se corretto, il server ritorna l'ultimo blocco (nuovo) 
            con tutte le transizioni<br> e come ultima transizione il "compenso" per la minatura del blocco
           """

@app.route('/chain')
def get_chain():
    """ ottienei tutta la chain """
    return jsonify(chain.chain), 200

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
