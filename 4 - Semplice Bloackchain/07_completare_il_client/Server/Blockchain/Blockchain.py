import hashlib
import uuid
import json
from time import time

__all__ = ["Blockchain", "hash"]

def hash(block):
    """ Esegue l'hash del blocco con SHA-256 """
    raw_block = json.dumps(block, sort_keys=True).encode()
    return hashlib.sha256(raw_block).hexdigest()

class Blockchain(object):
    def __init__(self):
        self.chain = []
        self.transactions = []
        self.address = uuid.uuid4().hex    
        self.new_block(previous_hash=1, nonce = 0)
        
    def new_block(self, previous_hash=None, nonce = "0"):
        """ crea un nuovo blocco """
        block = {
            'index': len(self.chain) + 1,
            'timestamp': time(),
            'previous_hash': previous_hash or hash(self.chain[-1]),
            'nonce': nonce,
            'transactions': self.transactions,
        }
        self.chain.append(block)
        self.transactions = []
        return block
 
    def add_transaction(self, sender, recipient, amount):
        """ 
        Aggiunge un movimento alla lista dei movimenti
        
        sender:     da
        recipient:  a
        amount:     quanto ($$$ soldi $$)
        """
        self.transactions.append({
                'sender': sender,
                'recipient': recipient,
                'amount': amount,
            })
        return self.last_block['index'] + 1
        
    @property
    def last_block(self):
        """ Per trovare l'ultimo blocco della catena """
        return self.chain[-1]
    
    def verify_reward(self, miner_nonce, miner_address):
        """ Verifca il nonce dell'ultimo blocco, e in caso assegna il coin di premio """
        mined_block = self.last_block.copy()
        mined_block["nonce"] = miner_nonce
        try_hash = str(hash(mined_block))
        if try_hash[:4] == "0000":
            self.new_block(nonce = miner_nonce)
            self.add_transaction(self.address, miner_address, 1)
            return True
        return False
        

