# Preparativi

## Pip

Di che abbiamo bisogno?

```sh
pip install Flask requests
```

## requests

Serve per inviare richieste http tramite python

### Esempi

Tassi di scambio delle monete; publicato dalla BCE

```python
import requests

r = requests.get("https://api.exchangeratesapi.io/latest")
r.status_code
r.headers['content-type']
r.text # è una non formattata
# Richiesta per conversione euro-dollaro
r = requests.get("https://api.exchangeratesapi.io/latest?symbols=USD")
r.json()
```

## Flask

Esempio di codice in *webapp.py*

```python
from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, World!'
```

Eseguire su windows

```sh
set FLASK_APP=webapp.py

python -m flask run
```

Possiamo testare sull'altra shell (se tutto va bene)

```python
r = requests.get("http://127.0.0.1:5000/")
r.text
```

Come si evince (se tutto è ben configurato) Flask può essere usato per creare webAPI mentre requests per interrogare tali API


