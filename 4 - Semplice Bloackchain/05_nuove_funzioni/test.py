import requests
import json
import uuid
from mine import *

# URL BASE
# Supponendo l'usuale porta 5000 di Flask su localhost
url_base = 'http://0.0.0.0:5000'

indirizzo_mio = uuid.uuid4().hex 

# Otteniamo il genesis_blok
def get_last_block():
    r = requests.get("{}/last_block".format(url_base))
    print("Ultimo blocco:\t{}\n".format(r.text))

get_last_block()

# Movimento di prova
transaction_parametri = {
    'sender': 'value1', 
    'recipient': 'value2',
    'amount': 3000
}

r = requests.post("{}/movimento".format(url_base), data=transaction_parametri)
print("Risposta server al Movimento di prova:\t{}\n".format(r.text))

# Mining

r = requests.get("{}/last_block".format(url_base))
block = json.loads(r.text)
nonce = mine(block)

transaction_parametri = {
    'nonce': nonce,
    'recipient': indirizzo_mio,
}

r = requests.post("{}/mine".format(url_base), data=transaction_parametri)
print("Risposta server al mining corretto:\t{}\n".format(r.text))

# Per vedere che è stato registrato controlliamo la lista movimenti...
print("indirizzo_mio:\t{}".format(indirizzo_mio))
r = requests.get("{}/ultimi_movimenti".format(url_base))
print("Ultimi movimenti:\n{}".format(r.text))

# Infine, otteniamo tutta la chain completa finora
r = requests.get("{}/chain".format(url_base))
print("Chain completa:\n{}".format(r.text))

# Ebbene sì, tutto bene quel che finisce bene...
# Ma il nostro server è solo soletto... Mentre la chain va distribuita in vari server!
# Così la replica dei dati impedisce che qualche furbetto giochi con le transazioni...
# Urgono correzioni e aggiunte nella prossima lezione!
