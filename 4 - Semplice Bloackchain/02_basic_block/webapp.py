import json
from time import time
from flask import Flask, jsonify, request

def hash(block):
    """ Esegue l'hash del blocco con SHA-256 """
    # Bisogna avere sort_keys=True, sennò se lasciato al python 
    # possiamo avere le chiavi in ordine sempre diverso,
    # il che non va bene, perchè sennò l'hashing non corrisponde!
    raw_block = json.dumps(block, sort_keys=True).encode()
    return hashlib.sha256(raw_block).hexdigest()

class Blockchain(object):
    def __init__(self):
        self.chain = []
        
        # Crea un "genesis block", ovvero il primo della catena
        self.new_block(previous_hash=1)
        
    def new_block(self, previous_hash=None):
        """ crea un nuovo blocco """
        block = {
            'index': len(self.chain) + 1,
            'timestamp': time(),
            'previous_hash': previous_hash or hash(self.chain[-1]),
        }
        self.chain.append(block)
        return block

    # Con questo decoratore si può accedere alla funzione come fosse una proprietà
    @property
    def last_block(self):
        """ Per trovare l'ultimo blocco della catena """
        return self.chain[-1]


app = Flask(__name__)
chain = Blockchain()

@app.route('/')
def root_path():
    """ root_path: messaggio di benvenuto """
    url_da_visitare = "{}last_block".format(request.base_url)
    return "Semplice Blockchain\n Prova ad accedere a <a href='{0}'>{0}</a>".format(url_da_visitare)

@app.route('/last_block')
def last_block():
    """ last_block: ottieni l'ultimo blocco """
    # Nota come si possa accedere alla funzione come proprietà
    return jsonify(chain.last_block), 200

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
