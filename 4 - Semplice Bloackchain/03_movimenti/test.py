import requests

# URL BASE
# Supponendo l'usuale porta 5000 di Flask su localhost
url_base = 'http://0.0.0.0:5000'

# Otteniamo l'ultimo blocco
def get_last_block():
    r = requests.get("{}/last_block".format(url_base))
    print("Ultimo blocco:\t{}\n".format(r.text))

get_last_block()

# Movimento di prova
transaction_parametri = {
    'sender': 'value1', 
    'receiver': 'value2',
    'amount': 3000
}

r = requests.post("{}/movimento".format(url_base), data=transaction_parametri)
print("Risposta server al Movimento di prova:\t{}\n".format(r.text))

get_last_block()

# Il risultato non è quello che aspettavamo...
# Certo, dobbiamo aggiungere una maniera per creare nuovi blocchi, dove i movimenti verranno registrati...
