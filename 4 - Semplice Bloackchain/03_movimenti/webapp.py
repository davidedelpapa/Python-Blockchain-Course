import json
from time import time
from flask import Flask, jsonify, request

def hash(block):
    """ Esegue l'hash del blocco con SHA-256 """
    raw_block = json.dumps(block, sort_keys=True).encode()
    return hashlib.sha256(raw_block).hexdigest()
        
class Blockchain(object):
    def __init__(self):
        self.chain = []
        # Lista dei movimenti da aggiungere al prossimo blocco
        self.transactions = []
        
        self.new_block(previous_hash=1)
        
    def new_block(self, previous_hash=None):
        """ crea un nuovo blocco """
        block = {
            'index': len(self.chain) + 1,
            'timestamp': time(),
            'previous_hash': previous_hash or hash(self.chain[-1]),
            # aggiungiamo la lista degli ultimi movimenti qui
            # Questo è il payload del blocco, ovvero i dati che ci interessano da registrare
            'transactions': self.transactions,
        }
        self.chain.append(block)
        
        # Riporta a 0 la lista dei movimenti
        self.transactions = []
        
        return block
    
    def add_transaction(self, sender, recipient, amount):
        """ 
        Aggiunge un movimento alla lista dei movimenti
        
        sender:     da
        recipient:  a
        amount:     quanto ($$$ soldi $$)
        """
        self.transactions.append({
                'sender': sender,
                'recipient': recipient,
                'amount': amount,
            })
        # La funzione ritorna l'id del prossimo blocco dove verrà salvato il movimento.
        # Siccome non esiste ancora, sarà l'ultimo blocco + 1.
        return self.last_block['index'] + 1
        
    @property
    def last_block(self):
        """ Per trovare l'ultimo blocco della catena """
        return self.chain[-1]


app = Flask(__name__)
chain = Blockchain()

@app.route('/')
def root_path():
    """ root_path: messaggio di benvenuto """
    url_ultimo_blocco = "{}last_block".format(request.base_url)
    url_movimento = "{}movimento".format(request.base_url)
    return "Semplice Blockchain<br> Ultimo blocco: <a href='{0}'>{0}</a><br> Registra movimento(POST) <a href='{1}'>{1}</a><br>".format(url_ultimo_blocco, url_movimento)

@app.route('/last_block')
def last_block():
    """ last_block: ottieni l'ultimo blocco """
    return jsonify(chain.last_block), 200

@app.route('/movimento', methods=['GET', 'POST'])
def set_transaction():
    """ registra un movimento """
    # Gestisce la registrazione tramite POST
    # Oppure se si usa GET dà istruzioni su usare POST e quali parametri
    if request.method == "POST":
        recipient = request.form.get('recipient')
        sender = request.form.get('sender')
        amount = request.form.get('amount')
        return "{}".format(chain.add_transaction(sender, recipient, amount))
    else:
        return """Usa il metodo POST per registrare un movimento<br>
        Parametri: <br>
        sender:     da<br>
        recipient:  a<br>
        amount:     quanti soldi<br>
        """

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
