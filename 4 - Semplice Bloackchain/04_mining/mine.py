import hashlib
import json

def mine(block):
    """ Trova la proof_of_work per il blocco """
    # Iniziamo con la proof a 0
    # La proof si chiama anche nonce in inglese
    nonce = 0
    while check_valid_proof(block, nonce) is False:
        # La proof non era valida, aggiungiamo 1 al nonce e si riprova
        nonce += 1
    
    # Finalmente la proof funziona...
    # Ritorniamo il nonce per controllo
    return nonce

def check_valid_proof(block, nonce):
    """ Controlla se la proof per il blocco è valida """
    # Aggiungiamo questa nonce al blocco
    block["nonce"] = str(nonce)
    
    # Facciamo l'hashing del blocco (trasformato in str per controllare facilmente i caratteri)
    try_hash = str(hash(block))
    
    # Controlliamo che le ultime cifre siano quattro 0
    return try_hash[:4] == "0000"


def hash(block):
    """ Esegue l'hash del blocco con SHA-256 """
    raw_block = json.dumps(block, sort_keys=True).encode()
    return hashlib.sha256(raw_block).hexdigest()
