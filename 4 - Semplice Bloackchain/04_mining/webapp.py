import hashlib
import uuid
import json
from time import time
from flask import Flask, jsonify, request

def hash(block):
    """ Esegue l'hash del blocco con SHA-256 """
    raw_block = json.dumps(block, sort_keys=True).encode()
    return hashlib.sha256(raw_block).hexdigest()

class Blockchain(object):
    def __init__(self):
        self.chain = []
        self.transactions = []
        # Address della chain (a random uuid in formato esadecimale)
        self.address = uuid.uuid4().hex
        
        self.new_block(previous_hash=1)
        
    def new_block(self, previous_hash=None, nonce = "0"):
        """ crea un nuovo blocco """
        block = {
            'index': len(self.chain) + 1,
            'timestamp': time(),
            'previous_hash': previous_hash or hash(self.chain[-1]),
            # aggiungiamo la proof of work
            'nonce': nonce,
            'transactions': self.transactions,
        }
        self.chain.append(block)
        
        self.transactions = []
        
        return block
 
    def add_transaction(self, sender, recipient, amount):
        """ 
        Aggiunge un movimento alla lista dei movimenti
        
        sender:     da
        recipient:  a
        amount:     quanto ($$$ soldi $$)
        """
        self.transactions.append({
                'sender': sender,
                'recipient': recipient,
                'amount': amount,
            })
        return self.last_block['index'] + 1
        
    @property
    def last_block(self):
        """ Per trovare l'ultimo blocco della catena """
        return self.chain[-1]  


app = Flask(__name__)
chain = Blockchain()

@app.route('/')
def root_path():
    """ root_path: messaggio di benvenuto """
    url_ultimo_blocco = "{}last_block".format(request.base_url)
    url_movimento = "{}movimento".format(request.base_url)
    return "Semplice Blockchain<br> Ultimo blocco: <a href='{0}'>{0}</a><br> Registra movimento(POST) <a href='{1}'>{1}</a><br>".format(url_ultimo_blocco, url_movimento)

@app.route('/last_block')
def last_block():
    """ last_block: ottieni l'ultimo blocco """
    return jsonify(chain.last_block), 200

@app.route('/movimento', methods=['GET', 'POST'])
def set_transaction():
    """ registra un movimento """
    if request.method == "POST":
        recipient = request.form.get('recipient')
        sender = request.form.get('sender')
        amount = request.form.get('amount')
        return "{}".format(chain.add_transaction(sender, recipient, amount))
    else:
        return """Usa il metodo POST per registrare un movimento<br>
        Parametri: <br>
        sender:     da<br>
        recipient:  a<br>
        amount:     quanti soldi<br>
        """

@app.route('/mine', methods=['GET', 'POST'])
def miner():
    """ propone il nonce minato per l'ultimo blocco """
    # Gestisce la registrazione del nonce minato tramite POST
    # Oppure se si usa GET dà istruzioni su usare POST e quali parametri
    if request.method == "POST":
        miner_nonce = request.form.get('nonce')
        mined_block = chain.last_block.copy()
        mined_block["nonce"] = miner_nonce
        try_hash = str(hash(mined_block))
        if try_hash[:4] == "0000":
            # Registriamo l'ultima transazione come dalla catena stessa al minatore, e doniamo 1 coin
            chain.new_block(nonce=miner_nonce)
            recipient = request.form.get('recipient')
            chain.add_transaction(chain.address, recipient, 1)
            return "Transaction correct", 200
        else:
            return "Error: nonce not working", 403
    else:
        return """Usa il metodo POST per minare l'ultimo blocco<br>
        Parametri: <br>
        nonce:      il nonce corretto dell'ultimo blocco<br>
        recipient:  dove accreditare il coin di "compenso"
        <br>
        Se corretto, il server ritorna l'ultimo blocco (nuovo) 
        con tutte le transizioni<br> e come ultima transizione il "compenso" per la minatura del blocco
        """

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
