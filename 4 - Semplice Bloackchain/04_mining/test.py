import requests
import json
import uuid
from mine import *

# URL BASE
# Supponendo l'usuale porta 5000 di Flask su localhost
url_base = 'http://0.0.0.0:5000'

indirizzo_mio = uuid.uuid4().hex 

# Otteniamo l'ultimo blocco
def get_last_block():
    r = requests.get("{}/last_block".format(url_base))
    print("Ultimo blocco:\t{}\n".format(r.text))

get_last_block()

# Movimento di prova
transaction_parametri = {
    'sender': 'value1', 
    'recipient': 'value2',
    'amount': 3000
}

r = requests.post("{}/movimento".format(url_base), data=transaction_parametri)
print("Risposta server al Movimento di prova:\t{}\n".format(r.text))


# Mining sbagliato
transaction_parametri = {
    'nonce': 0,
    'recipient': indirizzo_mio,
}

r = requests.post("{}/mine".format(url_base), data=transaction_parametri)
print("Risposta server al mining sbagliato:\t{}\n".format(r.text))


# Mining corretto

r = requests.get("{}/last_block".format(url_base))
block = json.loads(r.text)
nonce = mine(block)

transaction_parametri = {
    'nonce': nonce,
    'recipient': indirizzo_mio,
}

r = requests.post("{}/mine".format(url_base), data=transaction_parametri)
print("Risposta server al mining corretto:\t{}\n".format(r.text))

# Per vedere l'ultimo blocco...

get_last_block()

# Ma non c'è la transazione col coin che ci è stato dato!
# Certamente, ancora non è stato salvato in un nuovo blocco!
# Alla prossima lezione...

